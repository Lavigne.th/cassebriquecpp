#pragma once
#include "Point.h"
#include "Forme.h"

class Rectangle :public Forme
{
protected:
	Point p1, p2;
public:
	Rectangle();
	Rectangle(float, float, float, float);
	~Rectangle();
	void init(float, float, float, float);
	Point getp1() { return p1; }
	Point getp2() { return p2; }
	bool selection(float, float);
};