#include "Window.h"

Window::Window(const int WIDTH, const int HEIGHT) : WIDTH(WIDTH), HEIGHT(HEIGHT){
	init();
}

void Window::init()
{
	allegro_init();
	install_keyboard();   //installe le clavier
	install_mouse();      //installe la souris

	if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, 640, 480, 0, 0) != 0)
	{   //initialisation - r�solution de l'�cran (tailles 640-480)
		allegro_message(allegro_error);
		char arr[] = "mode graphique";
		allegro_message(arr);

		allegro_exit();
		return;
	}

	set_mouse_sprite(NULL);
	show_mouse(screen);
	set_keyboard_rate(0, 0);

	buffer = create_bitmap(SCREEN_W, SCREEN_H);

	if (!buffer)
	{
		char arr[] = ("pb bitmap");
		allegro_message(arr);
		allegro_exit();
		return;
	}
}

void Window::clear() {
	destroy_bitmap(buffer);
	set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
}


bool Window::getKey(int k) {
	return key[k];
}

Window::~Window() {

}