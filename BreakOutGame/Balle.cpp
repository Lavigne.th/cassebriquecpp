#include "Balle.h"

Balle::Balle() :p(200, 200), rayon(10)
{
	pasx = pasy = 0;
}

Balle::Balle(float px, float py, float pr, float ppas) : p(px, py), rayon(pr)
{
	pasx = pasy = ppas;
}

void Balle::affiche(BITMAP* page)
{
	circlefill(page, p.getx(), p.gety(), rayon, makecol(255, 0, 0));
}

int Balle::testlimite()
{
	if (p.getx() - rayon + pasx <= 0) return 1;
	if (p.getx() + rayon + pasx >= SCREEN_W)  return 3;
	if (p.gety() - rayon + pasy <= 0) return 2;
	if (p.gety() + rayon + pasy >= SCREEN_H) return 4;
	return 0;
}

void Balle::direction(int pface)
{
	if ((pface == 1) || (pface == 3)) pasx = -pasx;
	if ((pface == 2) || (pface == 4)) pasy = -pasy;
}

void Balle::deplace()
{
	p.deplace(pasx, pasy);
}
