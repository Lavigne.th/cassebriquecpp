#include "MurBrique.h"

MurBrique::MurBrique()
{
	float x, y;
	int i, j;
	for (x = 50, i = 1; i <= 6; i++, x = x + 100)
		for (y = 40, j = 1; j <= 3; j++, y = y + 70)
			l.push_back(new Brique(x, y, 45, 30, makecol(100, 0, 255), 8));

}

MurBrique::~MurBrique()
{
	for (it = l.begin(); it != l.end(); it++)
		delete (*it);
}

void MurBrique::affiche(BITMAP* page)
{
	for (it = l.begin(); it != l.end(); it++)
		(*it)->affiche(page);
}