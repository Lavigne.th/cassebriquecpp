#pragma once
#include <alegro/allegro.h>

class Forme
{
public:
	Forme();
	virtual ~Forme();
	virtual void affiche(BITMAP* page) = 0;
};
