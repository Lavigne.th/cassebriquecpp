#include "Brique.h"

Brique::Brique() :Rectangle()
{
	coul = makecol(0, 255, 0);
	nbcollision = 1;
}

Brique::Brique(float px, float py, float pl, float ph, long pcoul, int pnbcol) :Rectangle(px, py, pl, ph)
{
	coul = pcoul;
	nbcollision = pnbcol;
}

void Brique::affiche(BITMAP* page)
{
	rectfill(page, p1.getx(), p1.gety(), p2.getx(), p2.gety(), coul);
}