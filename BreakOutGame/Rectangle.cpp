#include "Rectangle.h"

Rectangle::Rectangle() :p1(), p2() { }

Rectangle::Rectangle(float px, float py, float pl, float ph) : p1(px, py), p2(px + pl, py + ph)
{ }

Rectangle::~Rectangle() { }

void Rectangle::init(float px1, float py1, float px2, float py2)
{
	p1.init(px1, py1);
	p2.init(px2, py2);
}

bool Rectangle::selection(float sx, float sy)
{
	if ((sx >= p1.getx()) && (sx <= p2.getx()) && (sy >= p1.gety()) && (sy <= p2.gety())) return true;
	return false;
}