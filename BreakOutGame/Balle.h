#pragma once
#include "Point.h"
#include "Forme.h"

class Balle :public Forme
{
protected:
	Point p;
	float rayon, pasx, pasy;
public:
	Balle();
	Balle(float, float, float, float);
	~Balle() {}
	void affiche(BITMAP* page);
	void direction(int);
	void deplace();
	bool selection(float, float);
	int testlimite();
};
