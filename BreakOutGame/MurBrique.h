#pragma once
#include <list>
#include "Forme.h"
#include "Brique.h"

class MurBrique :public Forme
{
protected:
	std::list<Brique*> l;
	std::list<Brique*>::iterator it;
public:
	MurBrique();
	~MurBrique();
	void affiche(BITMAP* page);
};