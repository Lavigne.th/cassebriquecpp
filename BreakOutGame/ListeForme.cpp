#include "ListForme.h"

ListeForme::ListeForme() {  }

ListeForme::~ListeForme() {  }

void ListeForme::affiche(BITMAP* buffer)
{
	for (it = l.begin(); it != l.end(); it++)
		(*it)->affiche(buffer);
}

void ListeForme::ajoute(Forme* p)
{
	l.push_back(p);
}

void ListeForme::supprime(Forme* p)
{
	delete p;         l.remove(p);
}