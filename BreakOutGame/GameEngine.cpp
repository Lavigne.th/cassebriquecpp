#include "GameEngine.h"


GameEngine::GameEngine(const int WIDTH, const int HEIGHT) {
	this->window = new Window(WIDTH, HEIGHT);

	liste.ajoute(brickWall = new MurBrique);
	liste.ajoute(ball = new Balle(300, 300, 7, 0.2));
	running = true;
}

void GameEngine::update() {
	if (window->getKey(KEY_ESC))
		running = false;
	ball->direction(ball->testlimite());

	ball->deplace();
}
void GameEngine::render() {
	clear_bitmap(window->getBuffer());
	
	liste.affiche(window->getBuffer());

	blit(window->getBuffer(), screen, 0, 0, 0, 0, window->getBuffer()->w, window->getBuffer()->h);
}

GameEngine::~GameEngine() {
	delete window;
}

void GameEngine::clear() {
	window->clear();
}