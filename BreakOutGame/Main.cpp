#include <iostream>
#include <math.h>
#include <alegro/allegro.h>

#include "GameEngine.h"


int main()
{
	GameEngine game(1280, 800);
	do
	{
		game.update();
		game.render();
	} while (game.isRunning());

	game.clear();

	return 0;

} END_OF_MAIN();