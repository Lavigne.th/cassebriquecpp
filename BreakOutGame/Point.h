#pragma once

class Point
{
protected:
	float x, y;
public:
	Point();
	Point(float, float);
	~Point();
	void deplace(float, float);
	void init(float, float);
	void affiche();
	float getx() { return x; }
	float gety() { return y; }
};