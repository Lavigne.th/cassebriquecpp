#pragma once
#include <alegro/allegro.h>

class Window {
private:
	const int WIDTH, HEIGHT;
	BITMAP* buffer;

	void init();
public:
	Window(const int WIDTH, const int HEIGHT);
	~Window();

	bool getKey(int key);
	void clear();

	BITMAP* getBuffer() { return this->buffer; }

};
 