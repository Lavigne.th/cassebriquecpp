#pragma once
#include "Rectangle.h"

class Brique :public Rectangle
{
protected:
	long coul;
	int nbcollision;
public:
	Brique();
	Brique(float, float, float, float, long, int);
	~Brique() {}
	void affiche(BITMAP* page);
	// virtual int action()=0;
};