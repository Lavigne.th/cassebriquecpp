#pragma once
#include "Window.h"
#include "ListForme.h"
#include "Balle.h"
#include "MurBrique.h"

class GameEngine {
private:
	Window* window;
	bool running;
	ListeForme liste;
	Balle* ball;
	MurBrique* brickWall;
public:
	GameEngine(const int WIDTH, const int HEIGHT);

	void update();
	void render();
	bool isRunning() { return running; };
	void clear();

	~GameEngine();

	Window* getWindow() { return this->window; }
};