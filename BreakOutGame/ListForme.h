#pragma once
#include <list>
#include <iostream>
#include <math.h>
#include <alegro/allegro.h>

#include "Forme.h"

class ListeForme
{
protected:
	std::list <Forme*> l;
	std::list<Forme*>::iterator it;
public:
	ListeForme();
	~ListeForme();
	void ajoute(Forme*);
	void supprime(Forme*);
	void affiche(BITMAP* buffer);
	int fsize() { return l.size(); }
};